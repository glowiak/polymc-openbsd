#!/bin/sh

set -e

# script to build PolyMC on OpenBSD

VERSION=${VERSION:-5.1}

export DEPENDS="jdk--%1.8 cmake extra-cmake-modules qt5 git minecraft lwjgl3"
export JAVA_HOME=/usr/local/jdk-1.8.0
export PMC_URL=https://github.com/PolyMC/PolyMC.git
export GIT_FLAGS="--recursive --single-branch"
export WORKDIR=$HOME/.tempPMCB
export CMAKE_FLAGS="-S . -B build -DCMAKE_INSTALL_PREFIX=install -DCMAKE_PREFIX_PATH=/usr/local/lib/qt5/cmake -DCMAKE_Java_COMPILER=$JAVA_HOME/bin/javac -DCMAKE_Java_JAVA_EXECUTABLE=$JAVA_HOME/bin/java -DCMAKE_Java_JAVAC_EXECUTABLE=$JAVA_HOME/bin/javac -DCMAKE_Java_JAR_EXECUTABLE=$JAVA_HOME/bin/jar -DCMAKE_Java_JAVADOC_EXECUTABLE=$JAVA_HOME/bin/javadoc -DCMAKE_Java_JAVAH_EXECUTABLE=$JAVA_HOME/bin/javah"
export LWJGL3_RUNTIME="https://codeberg.org/glowiak/openbsd-lwjgl3-runtime/raw/branch/master/lwjgl3-runtime"

echo "Installing dependiences..."
for i in $DEPENDS
do
	echo "Installing $i"
	su root -c "pkg_add $i"
done

echo "Downloading launcher source..."
rm -rf $WORKDIR
mkdir -p $WORKDIR
cd $WORKDIR
git clone $GIT_FLAGS -b ${VERSION} $PMC_URL
cd PolyMC

echo "Building launcher..."
cmake $CMAKE_FLAGS
cmake --build build -j4
cmake --install build
cmake --install build --component portable

echo "Downloading lwjgl3 runtime to /usr/local/bin/lwjgl3-runtime..."
ftp $LWJGL3_RUNTIME
su root -c "mv -f lwjgl3-runtime /usr/local/bin/lwjgl3-runtime && chmod 775 /usr/local/bin/lwjgl3-runtime"

echo "Building finished. You will find the launcher in $WORKDIR/PolyMC/install"
echo "The lwjgl2 runtime is at /usr/local/bin/minecraft"
echo "The lwjgl3 runtime is at /usr/local/bin/lwjgl3-runtime"
