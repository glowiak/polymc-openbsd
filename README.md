<img src="https://codeberg.org/glowiak/polymc-openbsd/raw/branch/master/Screenshot%20from%202023-07-18%2020-47-59.png">

# polymc-openbsd

PolyMC unlike MultiMC supports pretty much any OS you compile it on, and fixes bugs that prevent so.

This repo contains the script (build.sh) that will build the launcher for you.

Tested 100% works, you will find binaries in the releases tab.

A sidenote, don't use the 'PolyMC' executable. It is made for Linux and it does not work. Use bin/polymc instead.

You need to use custom runtimes.

Remember to check "Skip Java compatibility checks" in versions 1.13-1.17 when using my lwjgl3-runtime, or PMC will not let you run MC.